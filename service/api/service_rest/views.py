from .models import AutomobileVO, Technician, serviceAppointment
from common.json import ModelEncoder
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json


# Create your views here.


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number"]


class serviceAppointmentEncoder(ModelEncoder):
    model = serviceAppointment
    properties = ["vin", "name", "date_time", "technician", "reason", "id"]

    encoders = {"technician": TechnicianEncoder()}


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["color", "year", "vin", "import_href"]


@require_http_methods(["GET", "POST"])
def get_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianEncoder
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )


@require_http_methods(["GET", "POST"])
def make_appointment(request):

    if request.method == "GET":
        serviceappointment = serviceAppointment.objects.all()
        return JsonResponse(
            {"serviceappointment": serviceappointment},
            encoder=serviceAppointmentEncoder
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.get(id=content["technician"])
        content["technician"] = tech
        serviceappointment = serviceAppointment.objects.create(**content)
        return JsonResponse(
            serviceappointment,
            encoder=serviceAppointmentEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "PUT"])
def get_one_appointment(request, pk):
    if request.method == "DELETE":
        try:
            serviceappointment = serviceAppointment.objects.get(id=pk)
            serviceappointment.delete()
            return JsonResponse(
                serviceappointment,
                encoder=serviceAppointmentEncoder,
                safe=False
            )
        except serviceAppointment.DoesNotExist:
            return JsonResponse({"message": "service appointment does not exist"})

    elif request.method == "PUT":
        content = json.loads(request.body)
        serviceAppointment.objects.filter(id=pk).update(**content)
        return JsonResponse(
            content,
            encoder=serviceAppointmentEncoder,
            safe=False
        )