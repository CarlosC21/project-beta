import { useState } from "react";
import { NavLink } from "react-router-dom";
import "./index.css";

function Nav() {
  const [inventoryToggle, setInventoryToggle] = useState({
    inventory: "none",
    sales: "none",
    service: "none",
  });

  function handleInventory(e) {
    switch (e.target.innerHTML) {
      case "Inventory":
        if (inventoryToggle["inventory"] === "none") {
          setInventoryToggle({ ...inventoryToggle, inventory: "flex" });
        } else {
          setInventoryToggle({ ...inventoryToggle, inventory: "none" });
        }
        break;
      case "Sales":
        if (inventoryToggle["sales"] === "none") {
          setInventoryToggle({ ...inventoryToggle, sales: "flex" });
        } else {
          setInventoryToggle({ ...inventoryToggle, sales: "none" });
        }
        break;
      case "Service":
        if (inventoryToggle["service"] === "none") {
          setInventoryToggle({ ...inventoryToggle, service: "flex" });
        } else {
          setInventoryToggle({ ...inventoryToggle, service: "none" });
        }
        break;
    }
  }
  return (
    <nav className="navbar navbar-dark bg-secondary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          Car Project
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <h3 className="nav-bar-header" onClick={handleInventory}>
            Inventory
          </h3>
          <ul
            style={{ display: inventoryToggle["inventory"] }}
            className="navbar-nav me-auto mb-2 mb-lg-0"
          >
            <NavLink
              className="navbar-brand"
              to="inventory/manufacturer/create"
            >
              <li>New Manufacturer</li>
            </NavLink>
            <NavLink className="navbar-brand" to="inventory/manufacturers/list">
              <li>Manufacturers</li>
            </NavLink>
            <NavLink className="navbar-brand" to="inventory/models/new">
              <li>New Model</li>
            </NavLink>
            <NavLink className="navbar-brand" to="inventory/models/all">
              <li>Models</li>
            </NavLink>
            <NavLink className="navbar-brand" to="inventory/automobile/create">
              <li>New Automobile</li>
            </NavLink>
            <NavLink className="navbar-brand" to="inventory/automobiles/list">
              <li>Automobiles</li>
            </NavLink>
          </ul>
          <h3 className="nav-bar-header" onClick={handleInventory}>
            Sales
          </h3>
          <ul
            style={{ display: inventoryToggle["sales"] }}
            className="navbar-nav me-auto mb-2 mb-lg-0"
          >
            <NavLink className="navbar-brand" to="/sales/customer/new">
              <li>New Customer</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/sales/person/new">
              <li>New Sales Person</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/sales/records/new">
              <li>New Sales Record</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/sales/records/all">
              <li>All Sales Records</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/sales/records/search">
              <li>Search Sales Records</li>
            </NavLink>
          </ul>
          <h3 className="nav-bar-header" onClick={handleInventory}>
            Service
          </h3>
          <ul
            style={{ display: inventoryToggle["service"] }}
            className="navbar-nav me-auto mb-2 mb-lg-0"
          >
            <NavLink className="navbar-brand" to="/services/technician/enter">
              <li>New Technician</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/services/appointment/enter">
              <li>New Appointment</li>
            </NavLink>
            <NavLink className="navbar-brand" to="/services/appointments/list">
              <li>Appointment List</li>
            </NavLink>
            <NavLink
              className="navbar-brand"
              to="/services/appointments/history"
            >
              <li>Appointment History</li>
            </NavLink>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
