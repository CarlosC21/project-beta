import { BrowserRouter, Routes, Route } from "react-router-dom";
import AddCustomer from "./Sales/AddCustomer";
import AddSalesPerson from "./Sales/AddSalesPerson";
import AddSalesRecord from "./Sales/AddSalesRecord";
import CreateAutomobileInventory from "./Inventory/CreateInventoryItem";
import CreateManufacturer from "./Inventory/CreateManufacturer";
import CreateModel from "./Inventory/CreateModel";
import EnterServiceAppointment from "./Service/AddServiceAppointment";
import EnterTechnician from "./Service/AddTechnician";
import ListAppointments from "./Service/Appointments/ServiceAppointmentList";
import ListAutomobile from "./Inventory/ListInventoryItems";
import ListManufacturers from "./Inventory/ListManufacturers";
import ListModels from "./Inventory/ListModels";
import ListSales from "./Sales/ListSales";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SearchSalesRecords from "./Sales/SearchSalesRecords";
import ServiceAppointmentHistory from "./Service/Appointments/ServiceAppointmentHistory";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales/records/all" element={<ListSales />} />
          <Route
            path="/sales/records/search"
            element={<SearchSalesRecords />}
          />
          <Route path="/sales/customer/new" element={<AddCustomer />} />
          <Route path="/sales/records/new" element={<AddSalesRecord />} />
          <Route path="/sales/person/new" element={<AddSalesPerson />} />
          <Route
            path="services/technician/enter"
            element={<EnterTechnician />}
          />
          <Route
            path="services/appointment/enter"
            element={<EnterServiceAppointment />}
          />
          <Route
            path="services/appointments/list"
            element={<ListAppointments />}
          />
          <Route
            path="services/appointments/history"
            element={<ServiceAppointmentHistory />}
          />
          <Route
            path="inventory/manufacturers/list"
            element={<ListManufacturers />}
          />
          <Route
            path="inventory/manufacturer/create"
            element={<CreateManufacturer />}
          />
          <Route
            path="inventory/automobile/create"
            element={<CreateAutomobileInventory />}
          />
          <Route
            path="inventory/automobiles/list"
            element={<ListAutomobile />}
          />
          <Route path="/inventory/models/new" element={<CreateModel />} />
          <Route path="/inventory/models/all" element={<ListModels />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
