// search bar that takes a vin and returns a list of services given to that vin in the past

function SearchBar({ searchInput, submitSearch }) {
  const handleSearchInput = (event) => {
    const wordSearched = event.target.value;
    console.log(wordSearched);
    searchInput(wordSearched);
  };

  return (
    <div className="App">
      <input type="text" placeholder="Search" onChange={handleSearchInput} />
      <button type="submit" onClick={submitSearch}>
        Search VIN
      </button>
    </div>
  );
}

export default SearchBar;
