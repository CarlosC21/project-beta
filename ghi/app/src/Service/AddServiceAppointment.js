import React, { useState, useEffect } from "react";

function EnterServiceAppointment() {
  let [inputs, setInputs] = useState({
    vin: "",
    name: "",
    date_time: "",
    reason: "",
    technician: "",
  });
  let [technicians, setTechnicians] = useState([]);

  async function enterServiceAppointment(e) {
    e.preventDefault();
    let values = Object.values(inputs);

    if (values.includes("")) return;
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(inputs),
    };

    let result = await fetch(
      "http://localhost:8080/api/service/",
      requestOptions
    );

    if (result.ok) {
      setInputs({
        vin: "",
        name: "",
        date_time: "",
        reason: "",
        technician: "",
      });
    } else {
      console.log(result);
      console.log(
        "Error when posting to /api/technician in EnterTechnician function"
      );
    }
  }

  async function fetchTechnicians() {
    let technicianData = await fetch("http://localhost:8080/api/technicians/");
    let data = await technicianData.json();
    console.log("DATAAA", data);
    setTechnicians(data.technician);
  }

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    const val = e.target.value;
    setInputs({ ...inputs, [name]: val });
  }
  useEffect(() => {
    fetchTechnicians();
  }, []);

  return (
    <>
      <form>
        <div className="form-group">
          <label htmlFor="vin-input">Vin</label>
          <input
            type="text"
            name="vin"
            className="form-control"
            id="vin-input"
            value={inputs["vin"]}
            placeholder="Enter vin"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="name-input">Name</label>
          <input
            type="text"
            name="name"
            className="form-control"
            id="name-input"
            value={inputs["name"]}
            placeholder="name"
            onChange={handleInput}
          />
        </div>
        <div className="form-floating mb-3">
          <label htmlFor="date_time"></label>
          <input
            onChange={handleInput}
            value={inputs["date_time"]}
            placeholder="date/time"
            required
            type="date"
            name="date_time"
            id="date_time"
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label htmlFor="reason-input">Reason</label>
          <input
            type="text"
            name="reason"
            className="form-control"
            id="reason-input"
            value={inputs["reason"]}
            placeholder="reason"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="technician-input">Technician</label>
          <select
            onChange={handleInput}
            value={inputs["technician"]}
            name="technician"
            className="form-select"
          >
            <option value="" defaultValue={true}>
              Choose A Technician
            </option>
            {technicians.map((tech, index) => {
              return (
                <option key={tech.employee_number} value={index + 1}>
                  {tech.name}
                </option>
              );
            })}
          </select>
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={enterServiceAppointment}
        >
          Submit
        </button>
      </form>
    </>
  );
}

export default EnterServiceAppointment;
