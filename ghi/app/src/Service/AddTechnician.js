import React, { useState } from "react";

function EnterTechnician() {
  let [inputs, setInputs] = useState({ name: "", employee_number: "" });

  async function enterTechnician(e) {
    e.preventDefault();
    let values = Object.values(inputs);

    if (values.includes("")) return;
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(inputs),
    };

    let result = await fetch(
      "http://localhost:8080/api/technicians/",
      requestOptions
    );

    if (result.ok) {
      setInputs({ name: "", employee_number: "" });
    } else {
      console.log(result);
      console.log(
        "Error when posting to /api/technician in EnterTechnician function"
      );
    }
  }

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    const val = e.target.value;
    setInputs({ ...inputs, [name]: val });
  }

  return (
    <>
      <form>
        <div className="form-group">
          <label htmlFor="name-input">Name</label>
          <input
            type="text"
            name="name"
            className="form-control"
            id="name-input"
            value={inputs["name"]}
            placeholder="Enter name"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="number-input">Employee Number</label>
          <input
            type="number"
            name="employee_number"
            className="form-control"
            id="number-input"
            value={inputs["employee_number"]}
            placeholder="Enter employee number"
            onChange={handleInput}
          />
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={enterTechnician}
        >
          Submit
        </button>
      </form>
    </>
  );
}

export default EnterTechnician;
