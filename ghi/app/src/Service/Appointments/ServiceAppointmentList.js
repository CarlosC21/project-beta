// Lists all available service appointments by calling and saturating the AppointmentListItem component
import React, { useState, useEffect } from "react";
import AptListItem from "./AppointmentListItem";
function ListAppointments() {
  const [autos, setAutos] = useState([]);
  const [appointments, setAppointments] = useState([]);

  async function fetch_appointments() {
    let appointmentData = await fetch("http://localhost:8080/api/service/");
    let { serviceappointment } = await appointmentData.json();
    setAppointments(serviceappointment);
  }

  async function fetch_automobiles() {
    const automobileData = await fetch(
      "http://localhost:8100/api/automobiles/"
    );
    const { autos } = await automobileData.json();
    let vins = [];
    autos.forEach(({ vin }) => {
      if (vins.includes(vin)) return;
      vins.push(vin);
    });
    setAutos(vins);
  }

  useEffect(() => {
    fetch_appointments();
    fetch_automobiles();
  }, []);

  function removeAppointment(vin) {
    let updatedArray = [];
    appointments.forEach((app) => {
      if (app.vin === vin) {
        return;
      }
      updatedArray.push(app);
    });
    setAppointments(updatedArray);
  }

  function deleteAppointment(id, vin) {
    fetch(`http://localhost:8080/api/service/${id}/`, {
      method: "DELETE",
    }).then(() => {
      let updatedArray = [];
      appointments.forEach((app) => {
        if (app.vin === vin) {
          return;
        }
        updatedArray.push(app);
      });
      setAppointments(updatedArray);
    });
  }

  return (
    <>
      <table style={{ width: "100%" }}>
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        {appointments.map((app) => {
          console.log(app);
          let VIP = false;
          if (autos.includes(app.vin)) {
            VIP = true;
          }
          return (
            <AptListItem
              {...app}
              date={new Date(app.date_time).toLocaleDateString()}
              time={new Date(app.date_time).toLocaleTimeString("en-US")}
              deleteAppointment={deleteAppointment}
              vip={VIP}
            />
          );
        })}
      </table>
    </>
  );
}

export default ListAppointments;
