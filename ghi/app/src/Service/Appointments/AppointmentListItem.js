// defines the each active service appointment and contains logic to complete or cancel the apointment
import React from "react";
function AptListItem(props) {
  let color = props.vip ? "#ffe90063" : "";
  function testVIP() {
    if (props.vip) {
      return `⭐️⭐️⭐️${props.name}⭐️⭐️⭐️`;
    }
    return props.name;
  }
  return (
    <tr style={{ backgroundColor: color, height: "43px" }}>
      <td>{props.vin}</td>
      <td>{testVIP()}</td>
      <td>{props.date}</td>
      <td>{props.time}</td>
      <td>{props.technician.name}</td>
      <td>{props.reason}</td>
      <button
        onClick={() => {
          props.deleteAppointment(props.id, props.vin);
        }}
        style={{ marginTop: "2px", marginRight: "5px" }}
        class="btn btn-danger"
      >
        Cancel
      </button>
      <button
        onClick={() => {
          props.deleteAppointment(props.id, props.vin);
        }}
        style={{ marginTop: "2px" }}
        class="btn btn-success"
      >
        Finished
      </button>
    </tr>
  );
}

export default AptListItem;
