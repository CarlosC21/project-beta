import React, { useState, useEffect } from 'react';
// Contains a form that creates a sales person with a name and employee number

function AddSalesPerson() {
  //Body object for post request
  //Modified by onchange handler for form inputs
  let [inputs, setInputs] = useState({"name":null, "employee_number":null});

  async function createSalesPerson() {
    //Will not send post request if one or more values
    //is null (an input hasn't been changed before submit)
    let values = Object.values(inputs);

    if (values.includes(null)) return;

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(inputs)
    };

    let result = await fetch('http://localhost:8090/api/salesperson/', requestOptions);

    //Resets the input fields back to empty after successful
    //request is made (status 200)
    if (result.ok) {
      setInputs({"name":'', "employee_number":''});
    } else {
      console.log(result);
      console.log('Error when posting to /api/salesperson in createSalesPerson function.')
    }
  }

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    const val = e.target.value;
    setInputs({...inputs, [name]:val });
  }

  return(
<>
    <form>
      <div class="form-group">
        <label for="name-input">Name</label>
        <input type="text" name="name" class="form-control" id="name-input" value={inputs['name']} placeholder="Enter name" onChange={handleInput}/>
      </div>
      <div class="form-group">
        <label for="number-input">Employee Number</label>
        <input type="number" name="employee_number" class="form-control" id="number-input" value={inputs['employee_number']} placeholder="Enter employee number" onChange={handleInput}/>
      </div>
      <button type="submit" class="btn btn-primary" onClick={createSalesPerson}>Submit</button>
  </form>
  </>
  )
}

export default AddSalesPerson;