import React, { useState, useEffect } from 'react';

function AddSalesRecord() {
  //Body object for post request
  //Modified by onchange handler for form inputs
  let [inputs, setInputs] = useState({"automobile":'', "customer":'', "salesperson":'', "price":''});
  //List of all automobiles from inventory API
  let [autos, setAutos] = useState([]);
  //List of all salesperson objects from /api/salesperson/ endpoint
  let [salesPersons, setSalesPersons] = useState([]);
  //List of all employee numbers for each salesPersons obj
  //Prevents duplicate salesPersons
  let [employeeNums, setEmployeeNums] = useState([]);
  //List of all customer objects from /api/customer/ endpoint
  let [customers, setCustomers] = useState([]);
  //List of all customer phone numbers for each customers obj
  //Prevents duplication customers
  let [phoneNums, setPhoneNums] = useState([]);

  async function createSalesRecord(e) {
    //prevents page from reloading
    e.preventDefault();
    //Will not send post request if one or more values
    //is null (an input hasn't been changed before submit)
    let values = Object.values(inputs);

    if (values.includes(null)) return;

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(inputs)
    };
    let result = await fetch('http://localhost:8090/api/salerecord/', requestOptions);

    //Resets the input fields back to empty after successful
    //request is made (status 200)
    if (result.ok) {
      setInputs({"automobile":'', "customer":'', "salesperson":'', "price": ''});
    } else {
        console.log(result);
        console.log('Error when posting to /api/salerecord in createSalesRecord function.')
    }
  }

  async function fetchAutomobiles() {
    //Fetch and parse automobiles data from automobiles endpoint.
    let automobileData = await fetch('http://localhost:8100/api/automobiles/');
    let data = await automobileData.json();
    //Create new list of all automobiles from data fetched.
    let autoList = [];
    data.autos.forEach((auto) => {
      //If the automobile has already been added
      //continue on (don't add)
      if (autos.includes(auto.vin) || autoList.includes(auto.vin)) {
        return;
      }
      autoList.push(auto.vin);
    })
    //Combine newly created list with list established in state (autos)
    autoList = autoList.concat(autos);

    setAutos(autoList);
  }

  async function fetchSalesPersons() {
    //Fetch and parse saleperson data from saleperson API.
    let salespersonData = await fetch('http://localhost:8090/api/salesperson/');
    let { salesperson } = await salespersonData.json();
    //Create new list of all salespersons from data fetched.
    let salesPeople = [];
    //Create new list of all salespersons' employee numbers
    //to track and prevent duplicates
    let empNums = [];
    salesperson.forEach(sp => {
      //If the sale person has already been added
      //continue on (don't add)
      if (employeeNums.includes(sp.employee_number) || empNums.includes(sp.employee_number)) {
        return;
      }
      salesPeople.push(sp);
      empNums.push(sp.employee_number)
    })
    //Combine newly created list with list established in state (salesPersons)
    salesPeople = salesPeople.concat(salesPersons);
    empNums = empNums.concat(employeeNums);

    setSalesPersons(salesPeople);
    setEmployeeNums(empNums);
  }

  async function fetchCustomers() {
    //Fetch and parse customer data from customer API.
    let customerData = await fetch('http://localhost:8090/api/customer/');
    let { customer } = await customerData.json();
    //Create new list of all customers from data fetched.
    let customerList = [];
    //Create new list of all customers' phone numbers
    //to track and prevent duplicates
    let phoneNumbers = [];
    customer.forEach(c => {
      //If the customer has already been added
      //continue on (don't add)
      if (phoneNums.includes(c.phone_number) || phoneNumbers.includes(c.phone_number)) {
        return;
      }
      phoneNumbers.push(c.phone_number);
      customerList.push(c);
    })
    //Combine newly created list with list established in state (customers, phoneNums)
    customerList = customerList.concat(customers);
    phoneNumbers = phoneNumbers.concat(phoneNums);

    setCustomers(customerList);
    setPhoneNums(phoneNumbers);
  }

  //Retrieve all automobile, salesperson, customer data from API on initalization of component.
  useEffect(() => {
    fetchAutomobiles();
    fetchSalesPersons();
    fetchCustomers();
  }, []);

  // //Creates default values for all form inputs in inputs state.
  // //See [inputs, setInputs] state for more information.
  // //All inputes are set to null upon initialization.
  // //Once data is fetched from API endpoints, the first value in each data array
  // //will become the default value.
  // useEffect(() => {
  //   if (autos.length > 0 && inputs["automobile"] == null) {
  //       setInputs({...inputs, "automobile":`/api/automobiles/${autos[0]}/`})
  //   }
  //   if (salesPersons.length > 0 && inputs["salesperson"] == null) {
  //       setInputs({...inputs, "salesperson":salesPersons[0].id})
  //   }
  //   if (customers.length > 0 && inputs["customer"] == null) {
  //       setInputs({...inputs, "customer":customers[0].id})
  //   }
  // }, [autos, salesPersons, customers, inputs])

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    let val = e.target.value;
    //price field for salesrecord model must be an integer
    if (e.target.name === 'price') {
        val = Number(val)
    }
    setInputs({...inputs, [name]:val });
  }

  return(
<>
    <form>
      <div className="form-group">
        <label htmlFor="automobile-input">Automobile</label>
        <select name="automobile" className="form-control" onChange={handleInput} value={inputs['automobile']}>
            <option value="">Choose an automobile</option>
            {autos.map(vin => {
                return <option key={vin} name="automobile" value={`/api/automobiles/${vin}/`}>{vin}</option>
            })}
        </select>
      </div>
      <div className="form-group">
        <label htmlFor="salesperson-input">Sales Person</label>
        <select name="salesperson" className="form-control" onChange={handleInput} value={inputs['salesperson']}>
        <option value="">Choose sales person</option>
            {salesPersons.map(sp => {
                return <option key={sp.id} name="salesperson" value={sp.id}>{sp.name}</option>
            })}
        </select>
      </div>
      <div className="form-group">
        <label htmlFor="customer-input">Customer</label>
        <select name="customer" className="form-control" onChange={handleInput} value={inputs['customer']}>
            <option value="">Choose customer</option>
            {customers.map(c => {
                return <option key={c.id} name="customer" value={c.id}>{c.name}</option>
            })}
        </select>
      </div>
      <div className="form-group">
        <label htmlFor="price-input">Price</label>
        <input type="text" name="price" className="form-control" id="price-input" value={inputs['price']} placeholder="Enter price" onChange={handleInput}/>
      </div>
      <button type="submit" className="btn btn-primary" onClick={createSalesRecord}>Submit</button>
  </form>
  </>
  )
}

export default AddSalesRecord;