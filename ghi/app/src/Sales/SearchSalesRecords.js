import React, { useState, useEffect } from 'react';
// component that searches thimport React, { useState, useEffect } from 'react';
// a component that lists all the available sales records
// You need to show a page that lists all sales showing the sales person's name
// and employee number, the purchaser's name, the automobile VIN, and the price of the sale.

function SearchSalesRecords() {
  //List of all sales from salesrecords endpoint
  const [sales, setSales] = useState([]);
  //List of salesrecords filtered by sPerson
  const [salesFilter, setSalesFilter] = useState([]);
  //The current sales person to filter salesrecords by
  const [sPerson, setSPerson] = useState();
  //List of sales persons to filter salerecords by
  const [sPersonList, setSPersonList] = useState([]);

  async function fetch_sales() {
    //Fetch and parse sale record data from endpoint
    let saleData = await fetch('http://localhost:8090/api/salerecord/');
    let { salerecord } = await saleData.json();
    //Create new list of *names* of all sales people from records fetched.
    let newSPList = [];
    salerecord.forEach((record) => {
      //If the sale person has already been added
      //to the sales person list, continue on (don't add)
      if (sPersonList.includes(record.salesperson.name) || newSPList.includes(record.salesperson.name)) {
        return;
      }
      newSPList.push(record.salesperson.name);
    })
    //Combine newly created list with list established in state (sPersonList)
    newSPList = newSPList.concat(sPersonList);

    //Attempt to set the sPerson to first sales person
    //in the sales person array
    if (newSPList.length > 0) {
      setSPerson(newSPList[0]);
    }
    setSPersonList(newSPList);
    setSales(salerecord);
  }

  function filterSales() {
   //If there are no sales, then there's
   //nothing to filter.
   if (sales.length === 0) return;

   //Create new list for sales records
   //whose salesperson is sPerson
   let newFilteredList = [];
    //Ensure the person to filter sales records by exists.
    if (sPerson !== null) {
      sales.forEach(record => {
        if (record.salesperson.name === sPerson) {
          newFilteredList.push(record);
        }
      })
    }
    setSalesFilter(newFilteredList);
  }

  //Handles sPerson state change.
  function changeFilterBy(e) {
    setSPerson(e.target.value);
  }

  //Retrieve all sales from API on initalization of component.
  useEffect(() => {
    fetch_sales();
  }, []);

  //If the sales are ever updated,
  //update the filtered list.
  useEffect(() => {
    filterSales();
  }, [sales]);

  //If the sPerson ever changes,
  //update the filtered list.
  useEffect(() => {
    filterSales();
  }, [sPerson])


  return (
    <>
      <select onChange={changeFilterBy}>
        {sPersonList.map(salesperson => {
          return <option>{salesperson}</option>
        })}
      </select>
      <table style={{width: '100%'}}>
        <tr>
          <th>Sales Person</th>
          <th>Employee Number</th>
          <th>Purchaser</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      {salesFilter.map(sale => {
        return (
          <tr className="sale">
            <td>{sale.salesperson.name}</td>
            <td>{sale.salesperson.employee_number}</td>
            <td>{sale.customer.name}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.price}</td>
          </tr>
        )
      })}
      </table>
    </>
  )
}

export default SearchSalesRecords;