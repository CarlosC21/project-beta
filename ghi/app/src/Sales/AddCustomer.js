import React, { useState } from "react";

function AddCustomer() {
  //Body object for post request
  //Modified by onchange handler for form inputs
  let [inputs, setInputs] = useState({
    name: null,
    address: null,
    phone_number: null,
  });

  async function createCustomer() {
    //Will not send post request if one or more values
    //is null (an input hasn't been changed before submit)
    let values = Object.values(inputs);

    if (values.includes(null)) return;

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(inputs),
    };

    let result = await fetch(
      "http://localhost:8090/api/customer/",
      requestOptions
    );

    //Resets the input fields back to empty after successful
    //request is made (status 200)
    if (result.ok) {
      setInputs({ name: "", address: "", phone_number: "" });
    } else {
      console.log(result);
      console.log(
        "Error when posting to /api/customer in createCustomer function."
      );
    }
  }

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    const val = e.target.value;
    setInputs({ ...inputs, [name]: val });
  }

  return (
    <>
      <form>
        <div class="form-group">
          <label for="name-input">Name</label>
          <input
            type="text"
            name="name"
            class="form-control"
            id="name-input"
            value={inputs["name"]}
            placeholder="Enter name"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="address-input">Address</label>
          <input
            type="text"
            name="address"
            class="form-control"
            id="address-input"
            value={inputs["address"]}
            placeholder="Enter address"
            onChange={handleInput}
          />
        </div>
        <div class="form-group">
          <label for="numer-input">Phone Number</label>
          <input
            type="text"
            name="phone_number"
            class="form-control"
            id="number-input"
            value={inputs["phone_number"]}
            placeholder="Enter number"
            onChange={handleInput}
          />
        </div>
        <button type="submit" class="btn btn-primary" onClick={createCustomer}>
          Submit
        </button>
      </form>
    </>
  );
}

export default AddCustomer;
