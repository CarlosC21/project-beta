import React, { useState, useEffect } from 'react';

function ListSales() {
  const [sales, setSales] = useState([]);

  async function fetch_sales() {
    //Fetches data array from salerecord endpoint and
    //sets it to sales state.
    let saleData = await fetch('http://localhost:8090/api/salerecord/');
    let data = await saleData.json();
    setSales(data.salerecord);
  }

  //Retrieve all sales from API on initalization of component.
  useEffect(() => {
    fetch_sales();
  }, [])

  return (
    <>
      <table style={{width: '100%'}}>
        <tr>
          <th>Sales Person</th>
          <th>Employee Number</th>
          <th>Purchaser</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      {sales.map(sale => {
        return (
          <tr className="sale">
            <td>{sale.salesperson.name}</td>
            <td>{sale.salesperson.employee_number}</td>
            <td>{sale.customer.name}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.price}</td>
          </tr>
        )
      })}
      </table>
    </>
  )
}

export default ListSales;