import React, { useState, useEffect } from 'react';

function ListModels() {
  const [models, setModels] = useState([]);

  async function fetch_models() {
    //Fetches data array from salerecord endpoint and
    //sets it to sales state.
    let saleData = await fetch('http://localhost:8100/api/models/');
    let {models} = await saleData.json();
    console.log(models)
    setModels(models);
  }

  //Retrieve all sales from API on initalization of component.
  useEffect(() => {
    fetch_models();
  }, [])

  return (
    <>
      <h1>Vehicle models</h1>
      <table style={{width: '100%'}}>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      {models.map(model => {
        return (
          <tr className="sale">
            <td>{model.name}</td>
            <td>{model.manufacturer.name}</td>
            <td><img src={model.picture_url} alt='' style={{maxWidth: '150px', maxHeight: '150px', objectFit: 'contain'}}/></td>
          </tr>
        )
      })}
      </table>
    </>
  )
}

export default ListModels;