import React, { useState, useEffect } from "react";

function CreateModel() {
  //Body object for post request
  //Modified by onchange handler for form inputs
  let [inputs, setInputs] = useState({
    name: null,
    manufacturer_id: null,
    picture_url: null,
  });
  let [manufacturers, setManufacturers] = useState([]);

  async function createModel(e) {
    //Prevent page reload
    e.preventDefault();
    //Will not send post request if one or more values
    //is null (an input hasn't been changed before submit)
    let values = Object.values(inputs);

    if (values.includes(null)) return;

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(inputs),
    };
    console.log(inputs);
    let result = await fetch(
      "http://localhost:8100/api/models/",
      requestOptions
    );

    //Resets the input fields back to empty after successful
    //request is made (status 200)
    if (result.ok) {
      setInputs({ name: "", manufacturer_id: "", picture_url: "" });
    } else {
      console.log(result);
      console.log(
        "Error when posting to /api/customer in createCustomer function."
      );
    }
  }

  async function fetchManufacturers() {
    const result = await fetch("http://localhost:8100/api/manufacturers/");
    const { manufacturers } = await result.json();
    setManufacturers(manufacturers);
  }

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    let val = e.target.value;
    if (name === "manufacturer_id") {
      val = Number(val);
    }
    setInputs({ ...inputs, [name]: val });
  }

  useEffect(() => {
    fetchManufacturers();
  }, []);

  return (
    <>
      <form>
        <div className="form-group">
          <label htmlFor="name-input">Name</label>
          <input
            type="text"
            name="name"
            className="form-control"
            id="name-input"
            value={inputs["name"]}
            placeholder="Enter name"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="address-input">Manufacturer</label>
          <select
            name="manufacturer_id"
            className="form-control"
            onChange={handleInput}
          >
            <option value={null}>Select a manufacturer</option>
            {manufacturers.map((m) => {
              return (
                <option key={m.id} value={Number(m.id)}>
                  {m.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="picture-input">Picture URL</label>
          <input
            type="text"
            name="picture_url"
            className="form-control"
            id="picture-input"
            value={inputs["picture_url"]}
            placeholder="Enter picture url"
            onChange={handleInput}
          />
        </div>
        <button type="submit" className="btn btn-primary" onClick={createModel}>
          Submit
        </button>
      </form>
    </>
  );
}

export default CreateModel;
