// contains a form to create a new manufacturer
import React, { useState } from "react";

function CreateManufacturer() {
  let [inputs, setInputs] = useState({
    name: "",
  });

  async function enterManufacturer(e) {
    e.preventDefault();
    let values = Object.values(inputs);

    if (values.includes("")) return;
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(inputs),
    };

    let result = await fetch(
      "http://localhost:8100/api/manufacturers/",
      requestOptions
    );

    if (result.ok) {
      setInputs({
        name: "",
      });
    } else {
      console.log(result);
      console.log(
        "Error when posting to /api/manufacturer in CreateManufacturer function"
      );
    }
  }

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    const val = e.target.value;
    setInputs({ ...inputs, [name]: val });
  }

  return (
    <>
      <form>
        <div className="form-group">
          <label htmlFor="name-input">Name</label>
          <input
            type="text"
            name="name"
            className="form-control"
            id="name-input"
            value={inputs["name"]}
            placeholder="name"
            onChange={handleInput}
          />
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={enterManufacturer}
        >
          Create
        </button>
      </form>
    </>
  );
}

export default CreateManufacturer;
