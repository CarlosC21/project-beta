// displays a list of Inventory items components
// also contains a link to the CreateInventory Form

//  the 'shell' that holds the list of manufacturers as well as a link to the CreateManufacturer form

import React, { useState, useEffect } from "react";

function ListAutomobile() {
  const [automobiles, setAutomobiles] = useState([]);

  async function fetch_automobiles() {
    //Fetches data array from salerecord endpoint and
    //sets it to sales state.
    let automobileData = await fetch("http://localhost:8100/api/automobiles/");
    let data = await automobileData.json();
    setAutomobiles(data.autos);
  }

  //Retrieve all sales from API on initalization of component.
  useEffect(() => {
    fetch_automobiles();
  }, []);

  return (
    <>
      <table style={{ width: "100%" }}>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
        </tr>
        {automobiles.map((car) => {
          return (
            <tr className="car">
              <td>{car.vin}</td>
              <td>{car.color}</td>
              <td>{car.year}</td>
              <td>{car.model.name}</td>
              <td>{car.model.manufacturer.name}</td>
            </tr>
          );
        })}
      </table>
    </>
  );
}

export default ListAutomobile;
