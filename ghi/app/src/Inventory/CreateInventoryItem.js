// Form to create an Inventory Item
import React, { useState, useEffect } from "react";

function CreateAutomobileInventory() {
  let [inputs, setInputs] = useState({
    color: "",
    year: "",
    vin: "",
    model_id: "",
  });
  let [models, setModels] = useState([]);

  async function enterAutomobile(e) {
    e.preventDefault();
    let values = Object.values(inputs);

    if (values.includes("")) return;
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(inputs),
    };
    console.log(inputs);
    let result = await fetch(
      "http://localhost:8100/api/automobiles/",
      requestOptions
    );

    if (result.ok) {
      setInputs({
        color: "",
        year: "",
        vin: "",
        model_id: "",
      });
    } else {
      console.log(result);
      console.log(
        "Error when posting to /api/technician in EnterTechnician function"
      );
    }
  }

  async function fetchModels() {
    let modelData = await fetch("http://localhost:8100/api/models/");
    let data = await modelData.json();
    setModels(data.models);
  }

  function handleInput(e) {
    e.preventDefault();
    const name = e.target.name;
    let val = e.target.value;
    if (name === "year" || name === "model_id") {
      val = Number(val);
    }
    setInputs({ ...inputs, [name]: val });
  }

  useEffect(() => {
    fetchModels();
  }, []);

  return (
    <>
      <form>
        <div className="form-group">
          <label htmlFor="color-input">Color</label>
          <input
            type="text"
            name="color"
            className="form-control"
            id="color-input"
            value={inputs["color"]}
            placeholder="Color"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="year-input">Year</label>
          <input
            type="text"
            name="year"
            className="form-control"
            id="year-input"
            value={inputs["year"]}
            placeholder="year"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="vin-input">VIN</label>
          <input
            type="text"
            name="vin"
            className="form-control"
            id="vin-input"
            value={inputs["vin"]}
            placeholder="VIN"
            onChange={handleInput}
          />
        </div>
        <div className="form-group">
          <label htmlFor="model-input">Choose a model</label>
          <select
            onChange={handleInput}
            name="model_id"
            className="form-select"
            value={inputs["model_id"]}
          >
            <option value="" defaultValue={true}>
              Choose a model
            </option>
            {models.map((modelo) => {
              return (
                <option key={modelo.name} value={modelo.id}>
                  {modelo.name}
                </option>
              );
            })}
          </select>
        </div>
        <button
          type="submit"
          className="btn btn-primary"
          onClick={enterAutomobile}
        >
          Create
        </button>
      </form>
    </>
  );
}

export default CreateAutomobileInventory;
