//  the 'shell' that holds the list of manufacturers as well as a link to the CreateManufacturer form
import React, { useState, useEffect } from "react";

function ListManufacturers() {
  const [manufacturers, setManufacturers] = useState([]);

  async function fetch_manufacturers() {
    //Fetches data array from salerecord endpoint and
    //sets it to sales state.
    let manufacturerData = await fetch(
      "http://localhost:8100/api/manufacturers/"
    );
    let data = await manufacturerData.json();
    setManufacturers(data.manufacturers);
  }

  //Retrieve all sales from API on initalization of component.
  useEffect(() => {
    fetch_manufacturers();
  }, []);

  return (
    <>
      <table style={{ width: "100%" }}>
        <tr>
          <th>Manufacturers</th>
        </tr>
        {manufacturers.map((manu) => {
          return (
            <tr className="manu">
              <td>{manu.name}</td>
            </tr>
          );
        })}
      </table>
    </>
  );
}

export default ListManufacturers;
