from django.db import models
from phone_field import PhoneField

# Create your models here.
class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()

class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=300)
    phone_number = PhoneField(blank=True, help_text='Contact phone number')

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

   
class SaleRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salerecord",
        on_delete=models.PROTECT
    )

    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="salerecord",
        on_delete=models.PROTECT
    )

    customer = models.ForeignKey(
        Customer,
        related_name="salerecord",
        on_delete=models.PROTECT
    )

    price = models.PositiveIntegerField()