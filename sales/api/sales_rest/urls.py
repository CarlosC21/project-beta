from django.urls import path
from .views import get_salesperson, get_customer, get_salerecord


urlpatterns = [
    path(
        "salesperson/",
        get_salesperson,
        name="get_salesperson",
    ),
    path(
        "customer/",
        get_customer,
        name="get_customer"
    ),
    path(
        "salerecord/",
        get_salerecord,
        name="get_salerecord"
    )
]
