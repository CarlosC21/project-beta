from .models import AutomobileVO, SaleRecord, SalesPerson, Customer
from common.json import ModelEncoder
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

# Create your views here.
class SalespersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "id", "phone_number"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["color", "year", "vin", "import_href"]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "price",
        "salesperson",
        "customer",
        "automobile"
        ]
    encoders = {"salesperson": SalespersonEncoder(),
                "customer": CustomerEncoder(),
                "automobile": AutomobileVOEncoder()}


@require_http_methods(["GET", "POST"])
def get_salesperson(request):
    if request.method == "GET":
        salesperson = SalesPerson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder
        )

    else:
        content = json.loads(request.body)

        salesperson = SalesPerson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False

        )

@require_http_methods(["GET", "POST"])
def get_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        for c in customer:
            phone_ser = getattr(c, "phone_number")
            setattr(c, "phone_number", str(phone_ser))
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
            safe=False
        )

    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False

        )

@require_http_methods(["GET", "POST"])
def get_salerecord(request):
    if request.method == "GET":
        salerecord = SaleRecord.objects.all()
        for record in salerecord:
            customer = getattr(record, "customer")
            stringified_number = getattr(customer, "phone_number")
            setattr(customer, "phone_number", str(stringified_number))
        return JsonResponse(
            {"salerecord": salerecord},
            encoder=SaleRecordEncoder,
            safe=False
        )

    else:
        content = json.loads(request.body)

        try:
            automobile = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = automobile

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=400
                )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400
                )
        try:
            salesperson = SalesPerson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=400
                )

        salerecord = SaleRecord.objects.create(**content)
        customer = getattr(salerecord, "customer")
        stringified_number = getattr(customer, "phone_number")
        setattr(customer, "phone_number", str(stringified_number))
        return JsonResponse(
            salerecord,
            encoder=SaleRecordEncoder,
            safe=False
        )