## Car Project

## Starting Project

- docker volume create beta-data
- docker compose build
- docker compose up

## Design

- Project consists of three microservices: service, sales that integrate with the inventory microservice
- This integration was done by creating an AutomobileVO in service and sales models with an already made Automobile model in inventory
- Fields in Automobile model included the VIN which is useful to connect if vehicle was purchased at distributor, is entitled to receive VIP treatment during appointments

## Features

- If customer bought vehicle at distributor and makes a service appointment, treated as a VIP client
- Search bar enabled in appointment history using VIN of vehicle
- In sales record filter enabled for each employee
- Enabled a 'cancel' or 'finished' button in Appointment List to create a a better experience for user

## Paths

| Method | Path                               | Description                               |
| ------ | ---------------------------------- | ----------------------------------------- |
| POST   | /api/sales/customer/new            | Create new sales customer                 |
| POST   | /api/sales/records/new             | Create new sales record                   |
| POST   | /api/sales/person/new              | Create new sales person                   |
| GET    | /api/sales/records/all             | Get all sales the records                 |
| POST   | /api/services/appointments/enter   | Creating a new service appointment        |
| GET    | /api/services/appointments/list    | List service appointments                 |
| GET    | /api/services/appointments/history | Filters through past service appointments |
| POST   | /api/inventory/models/new          | Creates a new model in the inventory      |
| GET    | /api/inventory/models/all          | Gets all the models from the inventory    |
